﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using number8.dealeron.Models;
using number8.dealeron.Models.Mapped;
using System;
using System.Collections.Generic;
using System.Linq;

namespace number8.dealeron.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class SalesTaxController : ControllerBase
  {
    private readonly ILogger<SalesTaxController> _logger;
    private readonly TaxContext _context;

    public SalesTaxController(ILogger<SalesTaxController> logger, TaxContext taxContext)
    {
      _logger = logger;
      _context = taxContext;
    }

    [HttpPatch]
    [Route("updatecart")]
    public Invoice UpdateCart(ProductRequest request)
    {
      var invoice = _context.Invoices.Include("SoldItems").FirstOrDefault(i => i.ID == request.InvoiceID);
      if (invoice == null)
        return null;

      var category = _context.Categories.Include("CategoryTaxes").FirstOrDefault(c => c.ID == request.Product.CategoryID);

      if (category == null)
        return null;

      var existentLine = invoice.SoldItems.FirstOrDefault(s => s.SoldItemID == request.Product.ID);

      if (existentLine != null)
      {
        existentLine.LineTax += GetLineTax(category.CategoryTaxes, request.Product.ListPrice, request.Quantity);
        existentLine.SalesPrice += (request.Quantity * request.Product.ListPrice) + existentLine.LineTax;
        existentLine.Quantity += request.Quantity;
        invoice.SalesTaxes = GetInvoiceTaxes(invoice);
        invoice.TotalSalesPrice = GetInvoiceTotal(invoice);
        invoice.Descriptions = GenerateDescriptions(invoice);
        _context.Update(existentLine);
        _context.Update(invoice);
        _context.SaveChanges();
        return invoice;
      }

      InvoiceLine line = new InvoiceLine();

      line.SoldItemID = request.Product.ID;
      line.LineTax = GetLineTax(category.CategoryTaxes, request.Product.ListPrice, request.Quantity);
      line.SalesPrice = (request.Quantity * request.Product.ListPrice) + line.LineTax;
      line.Quantity = request.Quantity;

      invoice.SoldItems.Add(line);
      invoice.SalesTaxes = GetInvoiceTaxes(invoice);
      invoice.TotalSalesPrice = GetInvoiceTotal(invoice);
      invoice.Descriptions = GenerateDescriptions(invoice);

      _context.InvoiceLines.Add(line);
      _context.Update(invoice);
      _context.SaveChanges();
      return invoice;
    }

    [HttpGet]
    [Route("getproducts")]
    public ItemsCollection GetProducts()
    {
      //Initialize Receipts / Invoices
      var invoice = new Invoice();
      _context.Invoices.Add(invoice);
      _context.SaveChanges();

      var collection = new ItemsCollection();
      collection.InvoiceID = invoice.ID;
      collection.Items = _context.Items.ToList();

      return collection;
    }

    private decimal GetLineTax(List<Tax> taxes, decimal listPrice, int quantity)
    {
      decimal lineTax = 0;

      foreach (var tax in taxes)
      {
        lineTax += (listPrice * tax.Value) / 100;
      }

      return Math.Round((quantity * lineTax), 2);
    }

    private decimal GetInvoiceTaxes(Invoice invoice)
    {
      decimal totalTax = 0;

      foreach (var line in invoice.SoldItems)
      {
        totalTax += line.LineTax;
      }

      return Math.Round(totalTax, 2);
    }

    private decimal GetInvoiceTotal(Invoice invoice)
    {
      decimal total = 0;

      foreach (var line in invoice.SoldItems)
      {
        total += line.SalesPrice;
      }

      return Math.Round(total, 2);
    }

    private List<string> GenerateDescriptions(Invoice invoice)
    {
      var descriptions = new List<string>();
      foreach (var line in invoice.SoldItems)
      {
        var product = _context.Items.FirstOrDefault(i => i.ID == line.SoldItemID);
        if (product == null) continue;
        var desc = $"{product.Description}: {line.SalesPrice} ({line.Quantity} @ {product.ListPrice})";
        descriptions.Add(desc);
      }
      return descriptions;
    }
  }
}
