import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-taxes',
  templateUrl: './taxes.component.html',
  styleUrls: ['./taxes.component.scss']
})
/** taxes component*/
export class TaxesComponent {
  httpClient: HttpClient;
  url: string;
  items: ProductRequest[];
  search = '';
  invoice: Invoice;
  descriptions: string[] = [];

  /** taxes ctor */
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.httpClient = http;
    this.url = baseUrl;

    this.httpClient.get<ItemsCollection>(this.url + 'salestax/getproducts').subscribe(result => {
      this.getItemsCompleted(result);
    }, error => console.error(error));
  }

  getItemsCompleted(collection: ItemsCollection) {
    this.items = collection.items.map(function (item) {
      return {
        invoiceID: collection.invoiceID,
        product: item,
        quantity: 1
      }
    })
  }

  addToInvoice(request: ProductRequest) {
    this.httpClient.patch<Invoice>(this.url + 'salestax/updatecart', request).subscribe(result => {
      this.getInvoiceCompleted(result);
    }, error => console.error(error));
  }

  getInvoiceCompleted(invoice: Invoice) {
    this.invoice = invoice;
    this.descriptions = invoice.descriptions;
  }
}

interface InvoiceLine {
  id: number;
  soldItem: Item;
  quantity: number;
  salesPrice: number;
}

interface Invoice {
  id: number;
  soldItems: InvoiceLine[];
  descriptions: string[];
  salesTaxes: number;
  totalSalesPrice: number;
}

interface Category {
  id: number;
  name: string;
  items: Item[];
  taxes: Tax[];
}

interface Item {
  id: number;
  description: string;
  listPrice: number;
  categoryID: number;
}

interface Tax {
  id: number;
  description: string;
  value: number;
}

interface ProductRequest {
  invoiceID: number;
  quantity: number;
  product: Item;
}

interface ItemsCollection {
  invoiceID: number;
  items: Item[];
}
