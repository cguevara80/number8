﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace number8.dealeron.Models.Mapped
{
  public class ProductRequest
  {
    public int InvoiceID { get; set; }
    public int Quantity { get; set; }
    public Item Product { get; set; }
  }
}
