﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace number8.dealeron.Models.Mapped
{
  public class ItemsCollection
  {
    public int InvoiceID { get; set; }

    public List<Item> Items { get; set; }
  }
}
