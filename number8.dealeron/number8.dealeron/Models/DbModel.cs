﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace number8.dealeron.Models
{
  public class Item
  {
    public int ID { get; set; }
    public string Description { get; set; }
    public decimal ListPrice { get; set; }
    public int CategoryID { get; set; }
  }

  public class Category
  {
    public int ID { get; set; }
    public string Name { get; set; }
    public List<Item> Items { get; set; }
    public List<Tax> CategoryTaxes { get; set; }
  }

  public class Tax
  {
    public int ID { get; set; }
    public string Description { get; set; }
    public decimal Value { get; set; }
  }

  public class Invoice
  {
    public int ID { get; set; }
    public List<InvoiceLine> SoldItems { get; set; }

    [NotMapped]
    public List<string> Descriptions { get; set; }

    [NotMapped]
    public decimal SalesTaxes { get; set; }
    public decimal TotalSalesPrice { get; set; }
  }

  public class InvoiceLine
  {
    public int ID { get; set; }
    public Item SoldItem { get; set; }

    public int SoldItemID { get; set; }
    public decimal SalesPrice { get; set; }

    [NotMapped]
    public decimal LineTax { get; set; }
    public int Quantity { get; set; }
  }
}
